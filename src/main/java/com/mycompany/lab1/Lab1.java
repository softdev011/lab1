/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;

/**
 *
 * @author growt
 */
import java.util.Scanner;

public class Lab1 {
    private static char[][] board;
    private static char currentPlayer;
    private static boolean isGameOver;

    public static void main(String[] args) {
        startGame();
        playGame();
    }

    private static void startGame() {
        board = new char[3][3];
        currentPlayer = 'X';
        isGameOver = false;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }

    private static void playGame() {
        Scanner scanner = new Scanner(System.in);

        while (!isGameOver) {
            displayBoard();
            System.out.println("Player " + currentPlayer + ", input your number (row [1-3] column [1-3]): ");
            int row = scanner.nextInt() - 1;
            int col = scanner.nextInt() - 1;
            makeMove(row, col);
            checkGameOver();
            swapPlayer();

        }

        displayBoard();
        if (currentPlayer == '-')
            System.out.println("It's a draw!");
        else
            swapPlayer();
            System.out.println("Player " + currentPlayer + " wins!");
 

    }
    private static void makeMove(int row, int col) {
        board[row][col] = currentPlayer;
    }
    private static void displayBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + "");
            }
            System.out.println();
        }
    }
    private static void swapPlayer() {
    if (currentPlayer == 'X') {
        currentPlayer = 'O';
    } else {
        currentPlayer = 'X';
    }
    }
    private static void checkGameOver() {
        // Check rows
        for (int row = 0; row < 3; row++) {
            if (board[row][0] != '-' && board[row][0] == board[row][1] && board[row][0] == board[row][2]) {
                isGameOver = true;
                return;
            }
        }

        // Check columns
        for (int col = 0; col < 3; col++) {
            if (board[0][col] != '-' && board[0][col] == board[1][col] && board[0][col] == board[2][col]) {
                isGameOver = true;
                return;
            }
        }

        // Check diagonals
        if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            isGameOver = true;
            return;
        }
        if (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            isGameOver = true;
            return;
        }

        // Check for a draw
        isGameOver = true;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == '-') {
                    isGameOver = false;
                    return;
                }
            }
        }
    }
}
